import 'dart:convert';
import 'dart:io';

import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:dispensario/utils/config.dart';
import 'package:flutter/material.dart';
import 'package:dispensario/models/colaborador.dart';
import 'package:http/http.dart' as http;

class RegistroColaborador extends StatefulWidget {
  @override
  _RegistroColaboradorState createState() => _RegistroColaboradorState();
}

class _RegistroColaboradorState extends State<RegistroColaborador> {
  final TextEditingController _nombresController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: mainUI(context),
    );
  }

  Scaffold mainUI(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Nuevo Colaborador",
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(45.0, 0.0, 45.0, 0.0),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 40.0),
                  child: Image.asset("assets/client.png"),
                ),
              ),
              TextField(
                controller: _nombresController,
                decoration: InputDecoration(
                    labelText: 'Ingrese su Nombre Completo',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _usernameController,
                decoration: InputDecoration(
                    labelText: 'Nombre de Usuario',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    labelText: 'Ingrese su Correo',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _passwordController,
                decoration: InputDecoration(
                    labelText: 'Contraseña',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
                obscureText: true,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                child: Container(
                  height: 55.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_nombresController.text.length == 0 ||
                          _usernameController.text.length == 0 ||
                          _emailController.text.length == 0 ||
                          _passwordController.text.length == 0)
                        Alert.show(context, "¡Completar los Campos!",
                            "Recuerda diligenciar los datos solicitados para continuar el registro");
                      else {
                        Colaborador c = new Colaborador();
                        c.nombreCompleto = _nombresController.text;
                        c.username = _usernameController.text;
                        c.email = _emailController.text;
                        c.password = _passwordController.text;
                        registrarColaborador(c);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Text("Registrarme",
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    color: Colors.lightBlue,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void registrarColaborador(Colaborador colaborador) async {
    try {
      await http.post(url + "usuarios/create",
          body: json.encode(colaborador.toJson()),
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          }).then((http.Response response) {
        if (response.statusCode == 200) {
          Navigator.pop(context);
        } else {
          Alert.show(context, "¡Algo no anda bien!",
              "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
        }
      });
    } catch (ex) {
      Alert.show(context, "¡Algo no anda bien!",
          "No es posible la conexión, intenta de nuevo más tarde");
    }
  }
}
