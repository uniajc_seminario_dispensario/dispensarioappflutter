import 'dart:convert';
import 'dart:io';

import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:dispensario/models/cliente.dart';
import 'package:dispensario/utils/auth.dart';
import 'package:dispensario/utils/config.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http; 

class Comprador extends StatefulWidget {
  @override
  _CompradorState createState() => _CompradorState();
}

class _CompradorState extends State<Comprador> {
  final TextEditingController _identificionController = TextEditingController();
  final TextEditingController _nombresController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: mainUI(context),
    );
  }

  Scaffold mainUI(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Nuevo Cliente",
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 50.0),
                  child: Image.asset("assets/client.png"),
                ),
              ),
              TextField(
                controller: _identificionController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: 'Ingrese la Identificación',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: _nombresController,
                decoration: InputDecoration(
                    labelText: 'Ingrese el Nombre Completo',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0))),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                child: Container(
                  height: 65.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_identificionController.text.length == 0 ||
                          _nombresController.text.length == 0)
                        Alert.show(context, "¡Completar los Campos!",
                            "Recuerda diligenciar los datos solicitados para el registro");
                      else {
                        Cliente s = new Cliente();
                        s.identificacion = _identificionController.text;
                        s.nombres = _nombresController.text;
                        registrarCliente(s);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Text("Registrar Cliente",
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    color: Colors.lightBlue,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void registrarCliente(Cliente cliente) {
    Auth.getToken().then((token) {
      try {
        http.post(url + "api/clientes", body: json.encode(cliente.toJson()), headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          "Auth": "Token " + token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            var responseJson = json.decode(response.body);
            Navigator.pop(context, Cliente.fromJson(responseJson));
          } else {
            Alert.show(context, "¡Algo no anda bien!",
                "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
          }
        });
      } catch (ex) {
        Alert.show(context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
      }
    });
  }
}
