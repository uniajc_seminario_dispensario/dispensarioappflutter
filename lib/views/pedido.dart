import 'dart:convert';
import 'dart:io';

import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:dispensario/models/carrito.dart';
import 'package:dispensario/models/cliente.dart';
import 'package:dispensario/models/medicamento.dart';
import 'package:dispensario/utils/auth.dart';
import 'package:dispensario/utils/config.dart';
import 'package:dispensario/views/cliente.dart';
import 'package:dispensario/views/dispensario.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class Pedido extends StatefulWidget {
  @override
  _PedidoState createState() => _PedidoState();
}

class _PedidoState extends State<Pedido> {
  Carrito carrito = new Carrito();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: mainUI(context),
    );
  }

  Scaffold mainUI(BuildContext contexto) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Dispensario'),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(
                Icons.exit_to_app,
                color: Colors.white,
              ),
              onPressed: () {
                Auth.logout(context);
              },
            ),
            new IconButton(
              icon: new Icon(
                Icons.person_add,
                color: Colors.white,
              ),
              onPressed: () {
                asociarCliente();
              },
            ),
            new IconButton(
              icon: new Icon(
                Icons.done,
                color: Colors.white,
              ),
              onPressed: () {
                Auth.getId().then((colId) {
                  carrito.colaborador = colId;
                  sendPedido();
                });
              },
            )
          ],
        ),
        floatingActionButton: new FloatingActionButton.extended(
          icon: Icon(Icons.add),
          label: Text("Agregar"),
          onPressed: () {
            nuevoItem();
          },
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 5, right: 5, top: 5),
          child: cards(),
        ));
  }

  Widget cards() {
    return ListView.builder(
        itemCount: carrito.medicamento.length,
        itemBuilder: (context, index) {
          return Dismissible(
            key: Key(carrito.medicamento[index].codigo.toString()),
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Icon(
                Icons.delete,
                color: Colors.white,
              ),
            ),
            onDismissed: (direction) {
              setState(() {
                carrito.medicamento.removeAt(index);
              });
            },
            direction: DismissDirection.endToStart,
            child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                    height: 100.0,
                    child: Row(children: <Widget>[
                      Container(
                        height: 100,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 2, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 260,
                                child: Text(
                                  carrito.medicamento[index].nombre,
                                  style: new TextStyle(
                                      color: Colors.blue, fontSize: 20.0),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                child: Container(
                                    width: 260,
                                    child: Text(
                                      carrito.medicamento[index].unidad,
                                      style: new TextStyle(color: Colors.green),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    )),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                                child: Container(
                                  width: 260,
                                  child: Text(
                                    carrito.medicamento[index].forma,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Color.fromARGB(255, 48, 48, 54)),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                                child: Container(
                                  child: Text(
                                    "Code: " +
                                        carrito.medicamento[index].codigo
                                            .toString(),
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.amber),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                          height: 100.0,
                          width: 70.0,
                          child: Center(
                              child: Container(
                                  height: 100,
                                  child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 2, 0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: 260,
                                            child: Text(
                                              "Cant",
                                              style: new TextStyle(
                                                  color: Colors.green,
                                                  fontSize: 14.0),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          Container(
                                            width: 260,
                                            child: TextField(
                                              keyboardType:
                                                  TextInputType.number,
                                              onChanged: (val) => carrito
                                                      .medicamento[index]
                                                      .cantidadCompra =
                                                  int.parse(val),
                                              style: new TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 30.0),
                                              decoration: InputDecoration(
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        ],
                                      )))))
                    ]))),
          );
        });
  }

  void nuevoItem() async {
    Medicamento med = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => Dispensario()));

    setState(() {
      if (med != null) carrito.medicamento.add(med);
    });
  }

  void asociarCliente() {
    var txtSearch = new TextEditingController();
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Busqueda de Cliente',
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                )),
            content: TextField(
              keyboardType: TextInputType.number,
              controller: txtSearch,
              decoration:
                  InputDecoration(hintText: "Ingresa la identificación"),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text('Buscar'),
                onPressed: () {
                  buscarClienteID(txtSearch.text);
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  buscarClienteID(text) async {
    Auth.getToken().then((token) {
      try {
        http.get(url + "api/clientes/" + text, headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          "Auth": "Token " + token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            if (response.body == "") {
              nuevoCliente();
            } else {
              var responseJson = json.decode(response.body);
              carrito.cliente = Cliente.fromJson(responseJson);
              Alert.show(context, "¡Bienvenido!", carrito.cliente.nombres);
            }
          } else {
            Alert.show(context, "¡Algo no anda bien!",
                "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
          }
        });
      } catch (ex) {
        Alert.show(context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
      }
    });
  }

  nuevoCliente() async {
    Cliente cli = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => Comprador()));
    if (cli != null) carrito.cliente = cli;
  }

  void sendPedido() async {
    try {
      Auth.getToken().then((token) {
        http.post(url + "api/pedidos",
            body: json.encode(carrito.toJson()),
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              "Auth": "Token " + token
            }).then((response) {
          if (response.statusCode == 200) {
            var responseJson = json.decode(response.body);
            carrito.id = Cliente.fromJson(responseJson).id;
            for (Medicamento m in carrito.medicamento) {
              sendItem(m);
                
            }
            setState(() {
                  Alert.show(context, "Gracias por su compra",
                      "Compra realizada éxitosamente");
                  carrito = new Carrito();
                });
          } else
            Alert.show(context, "¡Algo no anda bien!",
                "Tenemos un problemita en el servidor, intenta de nuevo más tarde");
        });
        
      });
    } catch (ex) {
      Alert.show(context, "¡Algo no anda bien!",
          "No es posible la conexión, intenta de nuevo más tarde");
    }
  }

  void sendItem(Medicamento m) {
    Auth.getToken().then((token) {
      http.post(url + "/api/items",
          body: json.encode({
            "cantidad": m.cantidadCompra,
            "medicamento_id": m.codigo,
            "pedido_id": carrito.id
          }),
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            "Auth": "Token " + token
          }).then((response) {
        return response.statusCode == 200 ? 1 : 0;
      });
    });
  }
}
