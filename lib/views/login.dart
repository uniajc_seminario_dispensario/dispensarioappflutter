//import 'dart:async';

import 'package:dispensario/common/platform/platformScaffold.dart';
import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:dispensario/utils/auth.dart';
import 'package:dispensario/views/registro_colaborador.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<Login> {
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PlatformScaffold(
        appBar: AppBar(
          title: Text(
            "Inicio Sesión",
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: Container(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            child: ListView(
              children: <Widget>[
                Container(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 50.0),
                    child: Text(
                      "Dispensador de Medicamentos",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 35.0,
                          color: Colors.lightBlue[800],
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                TextField(
                  controller: _userNameController,
                  decoration: InputDecoration(
                      labelText: 'USUARIO',
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                SizedBox(height: 12.0),
                TextField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                      labelText: 'CONTRASEÑA',
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                  obscureText: true,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  child: Container(
                    height: 65.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (_userNameController.text.length == 0 ||
                            _passwordController.text.length == 0)
                          Alert.show(context, "¡Algo no está bien!",
                              "Recuerda diligenciar las credenciales para continuar con el acceso");
                        else
                          Auth.login(context, _userNameController.text,
                              _passwordController.text);
                      },
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0)),
                      child: Text("Ingresar",
                          style:
                              TextStyle(color: Colors.white, fontSize: 22.0)),
                      color: Colors.lightBlue,
                    ),
                  ),
                ),
                SizedBox(height: 12.0),
                FlatButton(
                    child: new Text('¿Registrarse?',
                      style: TextStyle(
                      fontSize: 23.0,
                      fontFamily: 'Montserrat'
                    ),),
                    onPressed: (){
                      Navigator.push(
                      context, MaterialPageRoute(builder: (context) => RegistroColaborador()),
                      );
                    },
              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
