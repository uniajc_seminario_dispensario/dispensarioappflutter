import 'dart:convert';
import 'dart:io';
import 'package:dispensario/models/carrito.dart';
import 'package:dispensario/models/medicamento.dart';
import 'package:dispensario/utils/auth.dart';
import 'package:dispensario/utils/config.dart';
import 'package:flutter/material.dart';
import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:http/http.dart' as http;

class Dispensario extends StatefulWidget {
  @override
  _DispensarioState createState() => _DispensarioState();
}

class _DispensarioState extends State<Dispensario> {
  Icon actionSearch = Icon(Icons.search, color: Colors.white);
  Widget actBar = Text("Productos");
  TextEditingController txtSearch = TextEditingController();
  List<Medicamento> items = new List<Medicamento>();

  static List<Carrito> itemsDispensario = new List();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: mainUI(context),
    );
  }

  @override
  void initState() {
    super.initState();
    busquedaProductos("");
  }

  Scaffold mainUI(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: actBar,
          backgroundColor: Colors.blueGrey,
          actions: <Widget>[
            IconButton(
              icon: actionSearch,
              tooltip: 'Busqueda',
              onPressed: () {
                setState(() {
                  if (actionSearch.icon == Icons.search) {
                    actionSearch = Icon(Icons.cancel, color: Colors.red);
                    actBar = TextField(
                      textInputAction: TextInputAction.go,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '¿Que necesitas?'),
                      style: TextStyle(color: Colors.white, fontSize: 16),
                      controller: txtSearch,
                      onChanged: (text) {
                        busquedaProductos(text);
                        //Agregar();
                      },
                    );
                  } else {
                    actionSearch = Icon(Icons.search, color: Colors.white);
                    actBar = Text("Productos");
                  }
                });
              },
            )
          ],
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 5, right: 5, top: 5),
          child: cards(),
        ));
  }

  Widget cards() {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Dismissible(
              key: Key(items[index].codigo.toString()),
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
              onDismissed: (direction) {
                setState(() {
                  items.removeAt(index);
                });
              },
              direction: DismissDirection.endToStart,
              child: new InkWell(
                onTap: () {
                  Navigator.pop(context, items[index]);
                  print(items[index].codigo);
                },
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Container(
                    height: 100.0,
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 100.0,
                          width: 70.0,
                          child: Padding(
                              padding: EdgeInsets.only(left: 2),
                              child: Image.asset("assets/logo_phar.png")),
                        ),
                        Container(
                          height: 100,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 2, 0, 0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 260,
                                  child: Text(
                                    items[index].nombre,
                                    style: new TextStyle(
                                        color: Colors.blue, fontSize: 20.0),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                  child: Container(
                                      width: 260,
                                      child: Text(
                                        items[index].unidad,
                                        style:
                                            new TextStyle(color: Colors.green),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                                  child: Container(
                                    width: 260,
                                    child: Text(
                                      items[index].forma,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 15,
                                          color:
                                              Color.fromARGB(255, 48, 48, 54)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                                  child: Container(
                                    child: Text(
                                      "Code: " + items[index].codigo.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.amber),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ));
        });
  }

  busquedaProductos(text) async {
    Auth.getToken().then((token) {
      try {
        http.get(url + "api/productos", headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          "Auth": "Token " + token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            var responseJson =
                json.decode(response.body).cast<Map<String, dynamic>>();
            setState(() {
              this.items.clear();
              this.items = responseJson
                  .map<Medicamento>((json) => Medicamento.fromJson(json))
                  .toList();
            });
          } else {
            Auth.logout(context);
          }
        });
      } catch (ex) {
        Alert.show(context, "¡Algo no anda bien!",
            "No es posible la conexión, intenta de nuevo más tarde");
      }
    });
  }
}
