import 'dart:async';
import 'package:dispensario/common/platform/platformScaffold.dart';
import 'package:dispensario/utils/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final int splashDuration = 4;

  startTime() async {
    return Timer(Duration(seconds: splashDuration), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      Auth.getToken().then((token) {
        if (token == null)
          Navigator.of(context).pushReplacementNamed('/login');
        else
          Navigator.of(context).pushReplacementNamed('/pedido');
      });
    });
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    var drawer = Drawer();

    return PlatformScaffold(
        drawer: drawer,
        body: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 80),
                    child: Image.asset("assets/logo.png")),
                SizedBox(height: 20.0),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    alignment: FractionalOffset(0.5, 0.3),
                    child: Text(
                      "Dispensario de Medicamentos",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 40.0, color: Colors.black),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                  child: Text(
                    "UNIAJC 2019-2",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            )));
  }
}
