import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dispensario/common/widgets/alertdialog.dart';
import 'package:dispensario/models/colaborador.dart';
import 'package:dispensario/utils/config.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Auth {
  static void login(
      BuildContext context, String username, String password) async {
    Map<String, String> body = {
      'username': username,
      'password': password,
    };
    try {
      await http.post(url + "usuarios/login", body: json.encode(body), headers: {
        HttpHeaders.contentTypeHeader: "application/json"
      }).then((http.Response response) {
        var responseJson = json.decode(response.body);
        if (response.statusCode == 200) {
          var user = new Colaborador.fromJson(responseJson);
          saveCurrentLogin(user);
          Navigator.of(context).pushReplacementNamed('/pedido');
        } else {
          Alert.show(context, "¡Algo no anda bien!",
              "La combinación de credenciales suministradas, no son correctas. Por favor verifica nuevamente");
        }
      });
    } catch (ex) {
      Alert.show(context, "¡Algo no anda bien!",
          "No es posible la conexión, intenta de nuevo más tarde");
    }
  }

  static Future<String> getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString("token");
  }

   static Future<int> getId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt("id");
  }

  static saveCurrentLogin(Colaborador user) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('token', user.token);
    await preferences.setInt('id', user.id);
    await preferences.setString('nombre', user.nombreCompleto);
  }

  static logout(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    await Navigator.of(context).pushReplacementNamed('/login');
  }
}
