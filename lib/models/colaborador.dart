class Colaborador {
  int id;
  String nombreCompleto;
  String username;
  String email;
  String token;
  String password;

  Colaborador();

  Colaborador.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nombreCompleto = json['nombrecompleto'],
        username = json['username'],
        email = json['email'],
        token =  json['jwt'];
        

  Map<String, dynamic> toJson() => {
        'nombrecompleto': nombreCompleto,
        'username': username,
        'email': email,
        'password': password
      };
}
