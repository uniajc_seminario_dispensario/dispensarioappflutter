import 'package:dispensario/models/medicamento.dart';
import 'package:dispensario/models/cliente.dart';

class Carrito {
  int id;
  Cliente cliente = new Cliente();
  List<Medicamento> medicamento = new List<Medicamento>();
  String fecha;
  int colaborador;

  Carrito();
  Carrito.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        fecha = json['fecha'],
        colaborador = json['colaborador_id'];

  Map<String, dynamic> toJson() => {
        'cliente_id': cliente.id,
        'colaborador_id': colaborador,
        'fecha': DateTime.now().toString()
      };
}
