class Cliente {
  int id;
  String identificacion;
  String nombres;

  Cliente();
  Cliente.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        identificacion = json['identificacion'],
        nombres = json['nombrecompleto'];

  Map<String, dynamic> toJson() => {
        'identificacion': identificacion,
        'nombrecompleto': nombres,
      };
}
