class Medicamento {
  int codigo;
  String nombre;
  String descripcion;
  String via;
  String medida;
  String cantidad;
  String unidad;
  String forma;
  int cantidadCompra;

  Medicamento.fromJson(Map<String, dynamic> json)
      : codigo = json["idProducto"],
        nombre = json["nombre"],
        descripcion = json["descripcion"],
        via = json["via"],
        medida = json["medida"],
        cantidad = json["cantidad"],
        unidad = json["unidad"],
        forma = json["forma"];
}
