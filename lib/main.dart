import 'package:dispensario/views/cliente.dart';
import 'package:dispensario/views/dispensario.dart';
import 'package:dispensario/views/login.dart';
import 'package:dispensario/views/pedido.dart';
import 'package:dispensario/views/registro_colaborador.dart';
import 'package:dispensario/views/splashscreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    /*Colaborador c = new Colaborador("userName", "token", "email", 0);
    Auth.saveCurrentLogin(c);*/
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.lightBlue[800],
          accentColor: Colors.cyan[600],
          fontFamily: 'Montserrat',
        ),
        home: SplashScreen(),
        routes: <String, WidgetBuilder>{
          '/dispensario': (BuildContext context) => new Dispensario(),
          '/pedido': (BuildContext context) => new Pedido(),
          '/login': (BuildContext context) => new Login(),
          '/cliente': (BuildContext context) => new Comprador(),
          '/registro_colaborador': (BuildContext context) => new RegistroColaborador(),
        });
  }
}
